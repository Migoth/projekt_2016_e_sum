var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var car = new Schema({
    carID: String
});

module.exports = mongoose.model('Car', car);