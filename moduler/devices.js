var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var device = new Schema({
    telephoneNr: String,
    serialNr: String,
    deviceID: String
});

module.exports = mongoose.model('Device', device);