var mongoose    = require('mongoose');
var Schema      = mongoose.Schema;

var lendingRegistration = new Schema({
    lendingDate: Date,
    returnDate: Date,
    userOID: {type: Schema.ObjectId},
    deviceOID: {type: Schema.ObjectId}
});

module.exports = mongoose.model('lendingRegistration', lendingRegistration);