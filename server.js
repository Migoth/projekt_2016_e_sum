"use strict";

var bcrypt = require('bcryptjs');
var express = require('express');
var fs = require('fs');
var session = require('express-session');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');

// Used for uploads of employee images
var multer = require('multer');
var upload = multer({dest:'./views/images/employees/', fileFilter:fileFilter});
function fileFilter (req, file, cb){
    var type = file.mimetype;
    if (type == "image/png" || type == "image/jpg" || type == "image/jpeg") {
        cb(null, true);
    } else {
        cb(null, false);
    }
}

var app = express();

// =============================================================================
// INITIALIZATION
// =============================================================================
// Port, HTTP middleware etc.
app.set('port', (process.env.PORT || 8080)); // Set the port
app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());
app.use(session({secret: 'super_secret_sssshhhh_indianer', saveUninitialized: true, resave: true}));

// MongoDB and Mongoose - We pick the default Promise implementation
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://dev_user:eaa123@ds153677.mlab.com:53677/hjemmepleje_db');

// Model
var Device = require('./moduler/devices');
var LendingRegistration = require(__dirname+'/moduler/lendingRegistrations');
var User = require('./moduler/user');

var sess;


// =============================================================================
// ROUTES FOR OUR APP
// =============================================================================
var router = express.Router();
var pageTitle = "PDB Udlån";

// - - - - - - - - - - - - - -
// Default root response
// - - - - - - - - - - - - - -
app.use(express.static('views'));
app.set('view engine', 'hbs');
app.set('views', __dirname +'/views');

// - - - - - - - - - - - - - -
// Login pages on "/" and "/lendingLogin"
// - - - - - - - - - - - - - -
router.route('/').get(function (req,res) {
    // Destroy session if someone is already logged in (also used for logout)
    if (req.session.idName) {
        req.session.destroy();
    }
    // Render the login window
    res.render('login', {
        browserTitle: "Log ind - " + pageTitle,
        navigationTitle: pageTitle + " - Telefonbog",
        navigationLink: "/"
    });
});
app.post('/', function(req,res) {
    User.findOne({password: {$regex: "^"+req.body.name+"$", $options: "i"}}, function (err, result) {
        if (err) {
            res.status(550).end("Incorrect login credentials!");
        } else {
            if(result) {
                sess = req.session;
                sess.idName = req.body.name;
                sess.name = result.name;
                res.status(200).end('done');
            } else {
                res.status(550).end("Incorrect login credentials!");
            }
        }
    });
});
router.route('/lendingLogin').get(function (req,res) {
    // Destroy session if someone is already logged in (also used for logout)
    if (req.session.idName) {
        req.session.destroy();
    }
    // Render the directory login window
    res.render('login', {
        browserTitle: "Log ind - " + pageTitle,
        navigationTitle: pageTitle + " - Udlån og aflevering",
        navigationLink: "/lendingLogin"
    });
});

// - - - - - - - - - - - - - -
// Requests sent to other end points
// - - - - - - - - - - - - - -
router.route('/lending').get(function (req,res) {
    sess=req.session;
    if (sess.idName) {
        res.render('deviceList', {
            browserTitle: "Lån enheder - " + pageTitle,
            navigationTitle: pageTitle,
            pageTitle: 'Vælg en telefon (' + sess.name + ')'

        });
    } else {
        res.writeHead(302, {'Location': '/lendingLogin'});
        res.end();
    }
});

router.route('/return').get(function (req,res) {
    sess=req.session;
    if (sess.idName) {
        res.render('return', {
            browserTitle: pageTitle,
            navigationTitle: pageTitle,
            pageTitle: 'Dine enheder (' + sess.name + ')'

        });
    } else {
        res.writeHead(302, {'Location': '/lendingLogin'});
        res.end();
    }
});

router.route('/employees').get(function (req,res) {
    sess=req.session;
    if (sess.idName) {
        res.render('employeeList', {
            browserTitle: pageTitle,
            navigationTitle: pageTitle,
            pageTitle: sess.name

        });
    } else {
        res.writeHead(302, {'Location': '/'});
        res.end();
    }
});

// - - - - - - - - - - - - - -
// Requests sent to "/api/devices"
// - - - - - - - - - - - - - -
router.route('/api/devices/all')
    .get(function (req, res) {
        sess=req.session;
        if (sess.idName) {
            Device.aggregate({$sort: {deviceID: -1}}, function (err, devices) {
                if (err) {
                    res.status(404).send("No devices found...");
                } else {
                    res.status(200).json(devices);

                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });
router.route('/api/devices/phones')
    .get(function (req, res) {
        sess=req.session;
        if (sess.idName) {
            Device.aggregate([{ $match: {"telephoneNr": {"$exists": true}} },{ $sort: {deviceID: -1} }], function (err, devices) {
                if (err) {
                    res.status(404).send("No phones found...");
                } else {
                    res.status(200).json(devices);

                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });
router.route('/api/devices/tablets')
    .get(function (req, res) {
        sess=req.session;
        if (sess.idName) {
            Device.aggregate([{ $match: {"telephoneNr": {"$exists": false}} },{ $sort: {deviceID: -1} }], function (err, devices) {
                if (err) {
                    res.status(404).send("No tablets found...");
                } else {
                    res.status(200).json(devices);

                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });


// - - - - - - - - - - - - - -
// Requests sent to "/api/lendingregistrations"
// - - - - - - - - - - - - - -
router.route('/api/lendingregistrations') 
    .get(function (req, res) {
        sess=req.session;
        if (sess.idName) {
            LendingRegistration.aggregate([{$match:{"returnDate":{"$exists":false}}},{$lookup:{from:"users",localField:"userOID",foreignField:"_id",as:"user"}},{$lookup:{from:"devices",localField:"deviceOID",foreignField:"_id",as:"device"}},{"$unwind":"$user"},{"$unwind":"$device"},{$project:{"lendingDate":1, "user.name":1, "user.title" : 1, "user.picture":1, "deviceOID": 1, "device.deviceID":1, "device.telephoneNr":1}}], function (err, lendingregistrations) {
                if (err) {
                    res.status(404).send("No lending registration found...");
                } else {
                    res.status(200).json(lendingregistrations);
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    })
    .post(function(req, res){
        sess=req.session;
        if (sess.idName) {
            User.findOne({password: {$regex: "^"+sess.idName+"$", $options: "i"}}, function(err, item){
                if (err){
                    res.status(500).send("Error: " + err);

                } else {
                    var useroid = item._id;
                    var deviceoid = mongoose.Types.ObjectId(req.body.deviceOID);

                    var rentedProduct = new LendingRegistration({
                        lendingDate: new Date().toISOString(),
                        userOID: useroid,
                        deviceOID: deviceoid
                    });

                    LendingRegistration.findOneAndUpdate({deviceOID: deviceoid, returnDate: {$exists: false}}, {$set:{returnDate: new Date().toISOString()}}, function (err){
                        if (err){
                            res.status(500).send("Error: " + err);
                        }  else {
                            rentedProduct.save(function(err) {
                                    if (err) {
                                        res.status(500).send("Error: " + err);
                                    } else {
                                        res.status(201).send("Rental is saved");
                                    }
                                }

                            );
                        }
                    });
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    })
    .put(function(req, res){
        sess=req.session;
        if (sess.idName) {
            var deviceoid = mongoose.Types.ObjectId(req.body.deviceOID);
            LendingRegistration.findOneAndUpdate({deviceOID: deviceoid, returnDate: {$exists: false}}, {$set:{returnDate: new Date().toISOString()}}, function(err){
                if (err){
                    res.status(500).send("Error: " + err);
                } else {
                    res.status(200).send("Rental is updated");
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
});

router.route('/api/lendingregistrations/lentdevices')
    .post(function (req, res){
        sess=req.session;
        if (sess.idName) {
            var phoneID = req.body.phoneID;
            var tabletID = req.body.tabletID;

            LendingRegistration.findOne({$or: [{deviceOID: phoneID}, {deviceOID: tabletID}], returnDate: {$exists: false}}, function (err, item){
                if (err){
                    res.status(500).send("Error: " + err);
                } else {
                    if (item){
                        res.status(400).send("Warning: Lending registration(s) already exist with these ID('s)")
                    } else {
                        res.status(204).send("Lending registration(s) does not exist with these ID('s)")
                    }
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
});

router.route('/api/lendingregistrations/phones')
    .get(function (req, res) {
        sess=req.session;
        if (sess.idName) {
            LendingRegistration.aggregate([{$match:{"returnDate":{"$exists":false}}},{$lookup:{from:"users",localField:"userOID",foreignField:"_id",as:"user"}},{$lookup:{from:"devices",localField:"deviceOID",foreignField:"_id",as:"device"}},{"$unwind":"$user"},{"$unwind":"$device"},{$project:{"lendingDate":1, "user.name":1, "user.title" : 1, "user.picture":1, "device.deviceID":1, "device.telephoneNr":1}},{$match:{"device.telephoneNr":{"$exists":true}}}], function (err, lendingregistrations) {
                if (err) {
                    res.status(404).send("No lending registration found for phones...");
                } else {
                    res.status(200).json(lendingregistrations);
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });
router.route('/api/lendingregistrations/tablets')
    .get(function (req, res) {
        sess=req.session;
        if (sess.idName) {
            LendingRegistration.aggregate([{$match:{"returnDate":{"$exists":false}}},{$lookup:{from:"users",localField:"userOID",foreignField:"_id",as:"user"}},{$lookup:{from:"devices",localField:"deviceOID",foreignField:"_id",as:"device"}},{"$unwind":"$user"},{"$unwind":"$device"},{$project:{"lendingDate":1, "user.name":1, "user.title" : 1, "user.picture":1, "device.deviceID":1, "device.telephoneNr":1}},{$match:{"device.telephoneNr":{"$exists":false}}}], function (err, lendingregistrations) {
                if (err) {
                    res.status(404).send("No lending registration found for tablets...");
                } else {
                    res.status(200).json(lendingregistrations);
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });
router.route('/api/lendingregistrations/fromuser')
    .get(function(req, res){
        sess=req.session;
        if (sess.idName) {
            LendingRegistration.aggregate([{ $match: { "returnDate": { "$exists": false } } },{ $lookup: { from: "users", localField: "userOID", foreignField: "_id", as: "user" } },{ $lookup: { from: "devices", localField: "deviceOID", foreignField: "_id", as: "device" } },{ "$unwind" : "$user" },{ "$unwind" : "$device" },{ $project : { "lendingDate" : 1, "deviceOID": 1, "user.name" : 1, "user.picture" : 1, "user.password" : 1, "device.deviceID" : 1, "device.telephoneNr" : 1 } },{ $match: { "user.password": { $regex: new RegExp("^" + req.session.idName, "i") } } }], function(err, items){
               if (err){
                   res.status(404).send("No lending registration found for user...")
               } else {
                   res.status(200).json(items);
               }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });

router.route('/api/lendingregistrations/:employeeOID')
    .get(function(req, res){
        sess=req.session;
        if (sess.idName) {
            LendingRegistration.aggregate([{ $lookup: { from: "devices", localField: "deviceOID", foreignField: "_id", as: "device" } },{ "$unwind": "$device" },{ $project: { "lendingDate": 1, "returnDate": 1, "userOID": 1, "deviceOID": 1, "device.deviceID": 1, "device.telephoneNr": 1 } },{ $match: { "userOID": mongoose.mongo.ObjectId(req.params.employeeOID), "returnDate": { "$exists": false } } }], function(err, items1){
                if (err){
                    res.status(404).send("No lending registration found for user...")
                } else {
                    LendingRegistration.aggregate([{ $lookup: { from: "devices", localField: "deviceOID", foreignField: "_id", as: "device" } },{ "$unwind": "$device" },{ $project: { "lendingDate": 1, "returnDate": 1, "userOID": 1, "deviceOID": 1, "device.deviceID": 1, "device.telephoneNr": 1 } },{ $match: { "userOID": mongoose.mongo.ObjectId(req.params.employeeOID), "returnDate": { "$exists": true } } }], function(err, items2){
                        if (err){
                            res.status(404).send("No lending registration found for user...")
                        } else {
                            res.status(200).json(items2.concat(items1));
                        }
                    });
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });


// - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - - - - - - - - - - - - - - -
// - - - - - - - - - - - - - - - - - - - - - - - - -


// - - - - - - - - - - - - - -
// REQUEST SENT TO THE ADMINISTRATOR API
// - - - - - - - - - - - - - -

// - Login -
router.route('/admin')
    .get(function (req,res) {
        // Destroy session if someone is already logged in (also used for logout)
        if (req.session.idName) {
            req.session.destroy();
        }
        // Render the login window
        res.render('admin/adminLogin', {
            browserTitle: "[ADMIN] Log ind - " + pageTitle,
            navigationTitle: pageTitle + " - Admin",
            navigationLink:"/admin"
        });
    })
    .post(function(req,res) {
        User.findOne({password: {$regex: "^"+req.body.user+"$", $options: "i"}}, function (err, result) {
            if (err) {
                res.status(550).send("Incorrect login credentials!");
            } else {
                if (!result) {
                    res.status(550).end("Incorrect login credentials!");
                } else {
                    if (!result.adminPassword) {
                        res.status(401).send("Access denied...");
                    } else {
                        bcrypt.compare(req.body.pass, result.adminPassword, function(err, bcRes) {
                            if (bcRes === false) {
                                res.status(550).end("Incorrect login credentials!");
                            } else {
                                sess = req.session;
                                sess.idName = req.body.user;
                                sess.name = result.name;
                                sess.admin = true;
                                res.status(200).end('done');
                            }
                        });
                    }
                }
            }
        });
    });

// - Employees -
router.route('/admin/employees').get(function (req,res) {
    sess=req.session;
    if (sess.admin == true) {
        res.render('admin/adminEmployeeList', {
            browserTitle: "[ADMIN] " + pageTitle,
            navigationTitle: pageTitle,
            pageTitle: 'Medarbejderadministration'

        });
    } else {
        res.status(401).send("Access denied...");
    }
});

router.route('/admin/employee/:id').get(function (req,res) {
    sess=req.session;
    if (sess.admin == true) {
        User.findOne({"_id": mongoose.mongo.ObjectId(req.params.id)}, function (err, employee) {
            if (err) {
                res.status(404).send("No employee found with the specified ID...");
            } else {
                var isAdmin = (employee.adminPassword ? "Ja" : undefined);
                res.render('admin/adminEmployeeHistoryList', {
                    browserTitle: "[ADMIN] " + pageTitle,
                    navigationTitle: pageTitle,
                    pageTitle: 'Medarbejder og historik',
                    _id: employee._id,
                    imageName: employee.picture,
                    empName: employee.name,
                    empTitle: employee.title,
                    indianName: employee.password,
                    adminStatus: (employee.adminPassword ? "Ja" : "Nej"),
                    isAdmin: isAdmin
                });
            }
        });
    } else {
        res.status(401).send("Access denied...");
    }
});

router.route('/admin/edit/employee').get(function (req,res) {
    sess=req.session;
    if (sess.admin == true) {
        if (req.query.id) {
            // Generate page with pre filled employee information if the query parameter ID exists
            User.findOne({"_id": mongoose.mongo.ObjectId(req.query.id)}, function (err, employee) {
                if (err) {
                    res.status(401).send("Database error...");
                } else {
                    res.render('admin/adminEmployeeEdit', {
                        browserTitle: "[ADMIN] " + pageTitle,
                        navigationTitle: pageTitle,
                        empName: employee.name,
                        empTitle: employee.title,
                        empPass: employee.password,
                        customImg: employee.picture
                    });
                }
            });
        } else {
            res.render('admin/adminEmployeeEdit', {
                browserTitle: pageTitle,
                navigationTitle: pageTitle,
                newEmployee: true
            });
        }
    } else {
        res.status(401).send("Access denied...");
    }
});

router.route('/admin/Rented').get(function (req,res) {

    sess=req.session;
    if (sess.admin == true) {
        res.render('admin/adminRentList', {
            browserTitle: "[ADMIN] " + pageTitle,
            navigationTitle: pageTitle,
            pageTitle: 'Administration af udlånt materiale'

        });
    } else {
        res.status(401).send("Access denied...");
    }
});

router.route('/api/admin/employees').get(function (req, res) {
        sess=req.session;
        if (sess.admin == true) {
            User.aggregate({$sort: {name: -1}}, function (err, employees) {
                if (err) {
                    res.status(404).send("No employees found...");
                } else {
                    res.status(200).json(employees);
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });
router.route('/api/admin/employee').post(upload.single('empImg'), function(req, res) {
    sess=req.session;
    if (sess.admin == true) {
        // Check that a user with the specific login credentials does not already exist
        User.findOne({password: {$regex: "^"+req.body.empPass+"$", $options: "i"}}, function (err, result) {
            if (!result) {
                // Get image if one is submitted
                var imgName;
                if (req.file) {
                    imgName = req.file.filename;
                }

                // Create user as admin or as a normal user
                var usr;
                if (req.body.empAdmin) {
                    var salt = bcrypt.genSaltSync(10);
                    var hash = bcrypt.hashSync(req.body.empAdmin, salt);
                    usr = new User({
                        name: req.body.empName,
                        password: req.body.empPass,
                        picture: imgName,
                        title: req.body.empTitle,
                        adminPassword: hash
                    });
                } else {
                    usr = new User({
                        name: req.body.empName,
                        password: req.body.empPass,
                        title: req.body.empTitle,
                        picture: imgName
                    });
                }

                // Save user in MongoDB
                usr.save(function (err) {
                    if (err) {
                        res.status(500).send("Error: " + err);
                    } else {
                        res.status(201).send("User was successfully saved in database");
                    }
                });
            } else {
                res.status(400).send("En medarbejder med det indtastede indianernavn eksisterer allerede.");
            }
        });
    } else {
        res.status(401).send("Access denied...");
    }
});
router.route('/api/admin/employee/:id')
    .get(function (req, res) {
        sess=req.session;
        if (sess.admin == true) {
            User.find({"_id": mongoose.mongo.ObjectId(req.params.id)}, function (err, employees) {
                if (err) {
                    res.status(404).send("No employee found with the specified ID...");
                } else {
                    res.status(200).json(employees);
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    })
    .put(upload.single('empImg'), function(req,res) {
        sess=req.session;
        if (sess.admin == true) {
            // Get current employee info
            User.findOne({"_id": mongoose.mongo.ObjectId(req.params.id)}, function (err, dbInfo) {
                // Check that another user does not have the new indian name already
                User.findOne({password: {$regex: "^"+req.body.empPass+"$", $options: "i"}}, function (err, result) {
                    if (result) {
                        if (result._id != req.params.id) {
                            res.status(400).send("En medarbejder med det indtastede indianernavn eksisterer allerede!");
                            return
                        }
                    }

                    // Get image if one is submitted
                    if (req.file) {
                        var imgName = req.file.filename;
                        // Delete old image if we have a new one, and save the new one
                        if (imgName) {
                            User.update({"_id": mongoose.mongo.ObjectId(req.params.id)}, {"$set": {"picture": imgName}}, function (err) {
                                if (err) {
                                    res.status(500).send(err);
                                } else {
                                    fs.unlink("./views/images/employees/" + dbInfo.picture, function (err) {
                                        if (err) {
                                            console.log("Could not delete image '" + dbInfo.picture + "'");
                                        }
                                    });
                                }
                            });
                        }
                    }

                    // Update the rest of the user information
                    User.update({"_id": mongoose.mongo.ObjectId(req.params.id)}, {"$set": {"name": req.body.empName, "password": req.body.empPass, "title": req.body.empTitle}}, function (err) {
                        if (err) {
                            res.status(500).send(err);
                        } else {
                            res.status(204).send('Message id:' + req.params.id + ' updated');
                        }
                    });
                });
            });
        } else {
            res.status(401).send("Access denied...");
        }
    })
    .delete(function(req,res) {
        sess=req.session;
        if (sess.admin == true) {
            // Delete Lending Registrations
            User.findOne({"_id": mongoose.mongo.ObjectId(req.params.id)}, function (err, userInfo) {
                LendingRegistration.remove({userOID: mongoose.mongo.ObjectId(req.params.id)}, function(err) {
                    if (err) {
                        res.status(500).send("Database delete error...");
                    } else {
                        // Delete User
                        User.remove({_id: mongoose.mongo.ObjectId(req.params.id)}, function (err) {
                            if (err) {
                                res.status(500).send("Database delete error...");
                            } else {
                                fs.unlink("./views/images/employees/" + userInfo.picture, function (err) {
                                    if (err) {
                                        console.log("Could not delete image '" + userInfo.picture + "'");
                                    }
                                });
                                res.status(200).send("Deleted!");
                            }
                        });
                    }
                });
            });
        } else {
            res.status(401).send("Access denied...");
        }
    });

// - Devices -
router.route('/admin/devices').get(function (req,res) {
    sess=req.session;
    if (sess.admin == true) {
        res.render('admin/adminDeviceList', {
            browserTitle: pageTitle,
            navigationTitle: pageTitle,
            pageTitle: 'Administration af enheder'

        });
    } else {
        res.status(401).send("Access denied...");
    }
});
router.route('/api/admin/devices')
    .get(function (req, res) {
        sess=req.session;
        if (sess.admin == true) {
            Device.aggregate({$sort: {deviceID: -1}}, function (err, devices) {
                if (err) {
                    res.status(404).send("No devices found...");
                } else {
                    res.status(200).json(devices);
                }
            });
        } else {
            res.status(401).send("Access denied...");
        }
    })
    .post(function(req,res) {
        sess=req.session;
        if (sess.admin == true) {
            var deviceID = req.body.deviceID;
            var serialNr = req.body.deviceSerial;
            var telephoneNr = req.body.devicePhonenumber;

            var newDevice = new Device({
                telephoneNr: telephoneNr,
                serialNr: serialNr,
                deviceID: deviceID
            });
            Device.findOne({deviceID: deviceID}, function (err, item){
               if (!item){
                   newDevice.save(function (err){
                       if (err){
                           res.status(500).send("Fejl: " + err);
                       } else {
                           res.status(201).send("Enhed oprettet!");
                       }
                   });
               } else {
                   res.status(400).send("Device with " + deviceID + " already exists in database");
               }
            });

        } else {
            res.status(401).send("Access denied...");
        }
    })
    .put(function(req,res) {
        sess=req.session;
        if (sess.admin == true) {
            var oldID = req.body.oldID;

            var deviceID = req.body.deviceID;
            var serialNr = req.body.deviceSerial;
            var telephoneNr = req.body.devicePhonenumber;


                Device.findOne({deviceID: deviceID}, function(err, item){
                    if (!item || deviceID==oldID){
                        Device.findOneAndUpdate({deviceID: oldID}, {$set:{deviceID: deviceID, serialNr: serialNr, telephoneNr: telephoneNr}}, function(err){
                            if (err){
                                res.status(500).send("Error: " + err);
                            } else {
                                res.status(201).send("Device is updated");
                            }
                        });
                    }  else {
                        res.status(400).send("Device with " + deviceID + " already exists in database");
                    }
                });


        } else {
            res.status(401).send("Access denied...");
        }
    })
    .delete(function(req,res) {
        sess=req.session;
        if (sess.admin == true) {
            var deviceToRemoveID = req.body.deviceID;
            Device.findOne({_id: deviceToRemoveID}, function(err, item){
               if (!err){
                   var databaseID = item._id;
                   LendingRegistration.find({deviceOID: databaseID, returnDate: {"$exists": false}}).remove(function(){
                       Device.findOneAndRemove({_id: deviceToRemoveID}, function(err){
                           if (err){
                               res.status(500).send("Error: " + err);
                           } else {
                               res.status(204).send("Device is deleted")
                           }
                       });
                   });
               } else {
                   res.status(500).send("Error " + err);
               }
            });



        } else {
            res.status(401).send("Access denied...");
        }
    });


router.route('/admin/edit/device/').get(function (req,res) {
    if (sess.admin == true) {
        var deviceID = req.query.id;
        Device.findOne({_id: deviceID}, function(err, item){
            if (err){
                res.status(500).send("Error: " + err);
            } else {
                var devID;
                var serNr;
                var telNr;
                if (item){
                    devID = item.deviceID;
                    serNr = item.serialNr;
                    telNr = item.telephoneNr;
                }

                res.render('admin/adminDeviceEdit', {
                    browserTitle: pageTitle,
                    navigationTitle: pageTitle,
                    devID: devID,
                    devSerial: serNr,
                    devPhone: telNr
                });
            }
        });
    } else {
        res.statuts(401).send("Access denied...");
    }
});

router.route('/admin/device/:id').get(function (req,res) {
    sess=req.session;
    if (sess.admin == true) {
        Device.findOne({_id: req.params.id}, function(err, item){
            var type;
            var img;
            if (item.telephoneNr){
                type = "Telefon";
                img = "iphone.png";
            } else {
                type = "Tablet";
                img = "ipad.png";
            }

            res.render('admin/adminDeviceHistoryList', {
                browserTitle: "[ADMIN] " + pageTitle,
                navigationTitle: pageTitle,
                pageTitle: 'Enhedsegenskaber',
                _id: item._id,
                devID: item.deviceID,
                devSerial: item.serialNr,
                telephoneNumber: item.telephoneNr,
                imageName: img,
                type: type
            });
        });
    } else {
        res.status(401).send("Access denied...");
    }
});

router.route('/admin/devices/:devID').get(function (req,res) {
    LendingRegistration.aggregate([{
        $match: {
            deviceOID: mongoose.Types.ObjectId(req.params.devID)
        }
    },{
        $lookup: {
            from: "users",
            localField: "userOID",
            foreignField: "_id",
            as: "user"
        }
    },{
        "$unwind" : "$user"
    },{
        $project : {
            "lendingDate" : 1,
            "returnDate" : 1,
            "user.name" : 1,
            "user.picture" : 1
        }
    }], function(err, items){
        if (err){
            res.status(500).send("Error: " + err );
        } else {
            res.status(200).json(items);
        }
    });
});



// Mount the router to the app
app.use('/', router);


// =============================================================================
// START THE SERVER
// =============================================================================
var server=app.listen(process.env.PORT || 8080, function(){
    console.log("Express server listening on port %d in %s mode", this.address().port, app.settings.env)
});

module.exports=server;