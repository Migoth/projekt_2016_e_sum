/*
var assert = require('assert');
var product = require('../service');
var employeeList = require('../views/js/employeeList');
var login = require('../views/js/Login');




suite("Rent Product",function(){
    test("Rent a product with deviceID 8, to user 'Bob', and get a Lending object with the deviceOID 582b056ee7d085cd08292834",function(){
        assert.equal("582b056ee7d085cd08292834",product.rentProduct(8,"Bob"));
    });
    test("Try to rent a product, with deviceID 8, but with  user 'God' who doesn't exist",function(){
        assert.equal(404,product.rentProduct(8,"God"));
    });
});

suite("searchListPeople", function() {
    test("Reads the contact info of Bob, should return name: Bob, phone number: 87654321", function() {
        assert.equal("Bob", "87654321", employeeList.searchListPeople("Bob"));
    });
    test("returns null", function() {
        assert.equal(404, employeeList.searchListEmployee("test"));
    });
});


suite("LoginUser",function(){
    test("Tries to log in an exstisting user, and sucess",function(){
        assert.equal(200,login.login("tico4242"));
    });
    test("Tried to log in with an invalid user, and fails like the rat it is",function (){
        assert.equal(550,login.login("wondersBob"));
    });
});
*/
var request = require('supertest');
var server = require('../server');

suite("Post error on login",function(done){
    test('Post wrong username',function(done){
       request(server).post('/')
           .send({name:'wonderBob'})
           .expect(550,done)
    });

    test('Post a real username',function(done){
        request(server).post('/')
            .send({name:'admin'})
            .expect(200,done)
    })

    test('Post a part of the username',function(done){
        request(server).post('/')
            .send({name:'adm'})
            .expect(550,done);
    });
});

suite("Post error on admin login",function(done){
    test('post wrong username',function(done){
        request(server).post('/admin')
            .send({user:'dag101'})
            .expect(401,done);
    });

    test("post right username, but wrong password",function(done){
        request(server).post('/admin')
            .send({user:'admin',pass:'notpassword'})
            .expect(550,done)
    });

    test("post right username, and right password",function(done){
        request(server).post('/admin')
            .send({user:'admin',pass:'admin'})
            .expect(200,done)
    });

    test("post wrong username, but right password",function(done){
        request(server).post('/admin')
            .send({user:'adam',pass:'password'})
            .expect(550,done)
    });


});

suite("Get denied access on the blocked routes",function(done){

    test('Get blocked from the lending page',function(done){
        request(server).get('/lending')
            .expect(302,done)
    });

    test('Get blocked from the lending page',function(done){
        request(server).get('/lending')
            .expect(302,done)
    });

});