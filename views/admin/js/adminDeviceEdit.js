var type = "POST";
var oldID;

function initContent() {
    if ($("#aId").val()){
        type = 'PUT';
    }

    oldID = $("#aId").val();
    // Save button clicked
    $("form").on("submit", function(event) {
        event.preventDefault();


        var outputT = $("#outputT");
        var deviceID = $("#aId").val();
        var serialNumber = $("#aSerialNumber").val();
        var phonenumber;

        if ($("#aPhonenumber").val()!=""){
            phonenumber = $("#aPhonenumber").val();
        }

        var toSend = {deviceID: deviceID, deviceSerial: serialNumber, devicePhonenumber: phonenumber};
        if (oldID){
            toSend.oldID = oldID;
        }
        if (deviceID){
            if (serialNumber){
                $.ajax({url: '/api/admin/devices',
                        type: type,
                        data: toSend,
                        statusCode: {
                            201: function () {
                                outputT.removeClass('alert-danger');
                                outputT.addClass("alert alert-success animated fadeInUp").html("Opretter Enhed");
                                window.location.href="/admin/devices";
                            },
                            401: function () {
                                outputT.removeClass('alert alert-success');
                                outputT.addClass("alert alert-danger animated fadeInUp").html("Manglende rettigheder!");
                                window.location.href="/";
                            },
                            500: function () {
                                outputT.removeClass('alert alert-success');
                                outputT.addClass("alert alert-danger animated fadeInUp").html("Something went wrong.. Could not save to the database..");
                            },
                            400: function (){
                                outputT.removeClass('alert alert-success');
                                outputT.addClass("alert alert-danger animated fadeInUp").html("En enhed med ID " + deviceID + " eksisterer allerede i databasen");
                            }
                        }
                    }
                );
            } else {
                outputT.removeClass('alert alert-success');
                outputT.addClass("alert alert-danger animated fadeInUp").html("Indtast et serienummer");
            }
        } else {
            outputT.removeClass('alert alert-success');
            outputT.addClass("alert alert-danger animated fadeInUp").html("Indtast et ID");
        }

    });
}
$(initContent);
