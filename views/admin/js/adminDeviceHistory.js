var oid;

//Pager stuff
var pageElements = [];
//The maximum number of pages to choose from
var pagerMaxSize = 9;
//Amount of elements to be shown per page
var showAmount = 4;
var amountOfPages;


function initContent() {
    // Save the employee OID for later use in the different actions
    oid = $("body").data("oid");

    // Edit user button
    $("#editButton").on("click", function(){
        window.location.replace("/admin/edit/device?id=" + oid);
    });

    // Delete user button
    $("#deleteButton").on("click", function(){
        $("#deleteModal").modal("show");
    });

    $("#confirmDelete").on("click", function(){
        $.ajax({
            url: "/api/admin/devices",
            type: 'DELETE',
            data: {deviceID: oid},
            statusCode: {
                204: function(){
                    window.location.replace("/admin/devices");
                },
                500: function(){

                },
                401: function(){

                }
            }
        })
    });

        $.getJSON("/admin/devices/" + oid, function (data) {
            pageElements = data;

            //Fix the settings of the pager according to the data from the database
            if (showAmount > pageElements.length){
                showAmount = pageElements.length;
            }
            amountOfPages = Math.ceil(pageElements.length/showAmount);

            if (pagerMaxSize > amountOfPages){
                pagerMaxSize = amountOfPages;
            }
            renderPage();
        });
}
$(initContent);

function renderPage(){
        if (pageElements.length > 0) {
            updatePager();
        } else {
            $("#fejlInfo").html("<h3><b>Der er endnu ikke oprettet nogen udlån for denne enhed!</b></h3>");
        }
}

function updatePager(){
    var clicked;
    //Check if this method is called from a click on the pager
    if(typeof $(event.target).attr("value") != "undefined"){
        //Save the click event before triggering the next
        var clicked = event.target;
    }
    $.get("../adminDeviceHistory.hbs", function(lenReg) {
        //Set the pagenumber to 1 if this method was not called from clicking the pager
        var pageNumber = clicked ? Number($(clicked).attr("value")) : 1;
        $("#pageInfo").html("Viser side <b>" + pageNumber + "</b> ud af <b>" + amountOfPages + "</b>");
        var startPoint = pageNumber - (Math.ceil(pagerMaxSize/2));
        if (startPoint < 0){
            startPoint = 0;
        } else if (startPoint + pagerMaxSize > amountOfPages-1){
            startPoint = amountOfPages - pagerMaxSize;
        }
        //Render Page
        $("#lendingRegistrations").empty();
        for (var i = 0; i < showAmount; i++){
            var relativeIndex = i + ((pageNumber-1)*showAmount);
            if (typeof pageElements[relativeIndex] != "undefined") {
                var compiled = Handlebars.compile(lenReg);

                var html = compiled({
                    userName: pageElements[relativeIndex].user.name,
                    lendingDate: toDateString(pageElements[relativeIndex].lendingDate),
                    returnDate: toDateString(pageElements[relativeIndex].returnDate),
                    imageName: pageElements[i].user.picture ? pageElements[relativeIndex].user.picture : "silhouette.png"
                });
                $('#lendingRegistrations').append(html);
            }
        }

        //Update pager
        $("#pager").empty();
        var pager = $("#pager");
        pager.append("<li><a value='" + 1 + "'onclick='updatePager()'>" + "<<" + "</a></li>");
        pager.append("<li><a value='" + (pageNumber-1 ? pageNumber-1 : 1) + "' onclick='updatePager()'>" + "<" + "</a></li>");
        for(var i = startPoint; i < pagerMaxSize+startPoint; i ++){
            if (i+1 == pageNumber){
                pager.append("<li class='active'><a value='" + (i+1) + "' onclick='updatePager()'>" + (i+1) + "</a></li>");
            } else {
                pager.append("<li><a value='" + (i+1) + "' onclick='updatePager()'>" + (i+1) + "</a></li>");
            }

        }
        pager.append("<li><a value='" + (pageNumber >= amountOfPages ? amountOfPages : pageNumber+1) + "' onclick='updatePager()'>" + ">" + "</a></li>");
        pager.append("<li><a value='" + amountOfPages + "' onclick='updatePager()'>" + ">>" + "</a></li>");
    });
}

function toDateString(isoDate) {
    if (isoDate) {
        var date = new Date(isoDate);
        var year = date.getFullYear();
        var month = date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth();
        var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        return day + "/" + month + "/" + year + " " + hour + ":" + minute;
    } else {
        return undefined;
    }
}