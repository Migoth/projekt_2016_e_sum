// Create listeners for DOM events
function initContent() {
    // Save button clicked
    $("form").on("submit", function(event) {
        event.preventDefault();
        submit($(this));
    });
    
    // Preview selected image listener
    $("#empImg").on('change', updatePreview);
}
$(initContent);

// - - - - - - - - - - - - - - - - - - - - - - - - -

// Used for getting the value of a parameter in the URL
function GetURLParameter(sParam) {
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');
    for (var i = 0; i < sURLVariables.length; i++) {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam) {
            return sParameterName[1];
        }
    }
}

// Used to update the preview image
function updatePreview() {
    var preview = document.querySelector('img');
    var file = document.querySelector('input[type=file]').files[0];
    var reader = new FileReader();

    reader.addEventListener("load", function () {
        preview.src = reader.result;
    }, false);

    if (file) {
        reader.readAsDataURL(file);
    }
}

function submit(form) {
    var outputT = $("#outputT");
    var eName = $("#aUser").val().trim();
    var eTitle = $("#aTitle").val().trim();
    var ePass = $("#aPass").val().trim();
    if (eName != "") {
        if (eTitle != "") {
            if (ePass != "") {
                var selectedImage = $('#empImg').val();
                var ext = selectedImage.split('.').pop().toLowerCase();
                if (selectedImage == "" || $.inArray(ext, ['png','jpg','jpeg']) >= 0) {
                    // Check for editing or creating
                    var id = GetURLParameter('id');
                    if (id) {
                        // Put new information
                        $.ajax({
                                url: '/api/admin/employee/' + id,
                                type: 'PUT',
                                contentType: false,
                                processData: false,
                                data: new FormData(form[0]),
                                statusCode: {
                                    204: function () {
                                        outputT.addClass("alert alert-success animated fadeInUp").html("Opdaterer " + "<span style='text-transform:uppercase'>" + eName + "</span>");
                                        outputT.removeClass('alert-danger');
                                        window.location.href = "/admin/employees";
                                    },
                                    400: function (res) {
                                        outputT.removeClass('alert alert-success');
                                        outputT.addClass("alert alert-danger animated fadeInUp").html(res.responseText);
                                    },
                                    401: function () {
                                        outputT.removeClass('alert alert-success');
                                        outputT.addClass("alert alert-danger animated fadeInUp").html("Manglende rettigheder!");
                                        window.location.href = "/";
                                    },
                                    500: function () {
                                        outputT.removeClass('alert alert-success');
                                        outputT.addClass("alert alert-danger animated fadeInUp").html("Something went wrong.. Could not save to the database..");
                                    }
                                }
                            }
                        );
                    } else {
                        // Post new user info
                        $.ajax({
                                url: '/api/admin/employee',
                                type: 'POST',
                                contentType: false,
                                processData: false,
                                data: new FormData(form[0]),
                                statusCode: {
                                    201: function () {
                                        outputT.addClass("alert alert-success animated fadeInUp").html("Opretter " + "<span style='text-transform:uppercase'>" + eName + "</span>");
                                        outputT.removeClass('alert-danger');
                                        window.location.href = "/admin/employees";
                                    },
                                    400: function (res) {
                                        outputT.removeClass('alert alert-success');
                                        outputT.addClass("alert alert-danger animated fadeInUp").html(res.responseText);
                                    },
                                    401: function () {
                                        outputT.removeClass('alert alert-success');
                                        outputT.addClass("alert alert-danger animated fadeInUp").html("Manglende rettigheder!");
                                        window.location.href = "/";
                                    },
                                    500: function () {
                                        outputT.removeClass('alert alert-success');
                                        outputT.addClass("alert alert-danger animated fadeInUp").html("Something went wrong.. Could not save to the database..");
                                    }
                                }
                            }
                        );
                    }
                } else {
                    outputT.removeClass('alert alert-success');
                    outputT.addClass("alert alert-danger animated fadeInUp").html("Kun billeder i JPEG eller PNG formatet accepteres");
                }
            } else {
                outputT.removeClass('alert alert-success');
                outputT.addClass("alert alert-danger animated fadeInUp").html("Indtast et indianernavn");
            }
        } else {
            outputT.removeClass('alert alert-success');
            outputT.addClass("alert alert-danger animated fadeInUp").html("Indtast en jobtitel");
        }
    } else {
        outputT.removeClass('alert alert-success');
        outputT.addClass("alert alert-danger animated fadeInUp").html("Indtast et medarbejdernevnnavn");
    }
}