var oid;
var lrs;
var latestSelectedDeviceOID;

// jQuery selects
var addPoint;
var currentPageLbl;
var buttonNext;
var buttonPrev;
var dotsNext;
var dotsPrev;

// Pagination variables
var lrsPrPage = 6;
var paginationRadius = 5;
var currentPage = 1;
var numberOfPages;
var lrHbsElement;
var pgHbsElement;

function initContent() {
    // Save the employee OID for various later use
    oid = $("body").data("oid");

    // Same for the point where we add LRs every time we change page
    addPoint = $('#lendingRegistrations');

    // "Edit user" button listener
    $("#editButton").on("click", function(){
        window.location.replace("/admin/edit/employee?id=" + oid);
    });

    // "Delete user" button listener
    $("#deleteButton").on("click", function(){
        $("#deleteModal").modal("show");
    });

    // "Confirm user deletion" button listener
    $("#confirmDelete").on("click", function(){
        $.ajax({
            url: "/api/admin/employee/" + oid,
            type: 'DELETE',
            data: {employeeOID: oid},
            statusCode: {
                200: function(){
                    console.log("Success!");
                    window.location.replace("/admin/employees");
                },
                500: function(res){
                    console.log(res);
                },
                401: function(res){
                    console.log(res);
                }
            }
        })
    });

    // Get all the lending registrations
    $.get("../adminEmployeeHistory.hbs", function(lenRegElement) {
        // Save the handlebars element for later use upon page changes
        lrHbsElement = lenRegElement;

        $.getJSON("/api/lendingregistrations/" + oid, function (data) {
            // Save all the lending registrations so we don't need to ask the DB every time we change page
            lrs = data;
            numberOfPages = Math.ceil(data.length/lrsPrPage);

            // Add pagination if we have enough results
            if (numberOfPages > 1) {
                currentPageLbl = $("#currentPage");
                currentPageLbl.text("1");
                $("#totalPages").text(numberOfPages);
                addPagination();
            } else {
                $("#pageInfo").remove();
            }

            // Add lending registration elements
            addLendingRegistrations(false);

            // Listen for "Return device" button clicks
            $("#lendingRegistrations").on("click", ".inListBtn", function() {
                latestSelectedDeviceOID = $(this).data('deviceoid');
                $("#deviceModalText").text("Vil du aflevere enheden med id " + $(this).data('deviceid') + "?");
                $("#deviceModal").modal("show");
            });
        });
    });

    // Button for confirming a return of a device
    $("#confirmReturnButton").on("click", function() {
        $.ajax({
            url: "/api/lendingregistrations",
            type: "PUT",
            dataType: "json",
            data: {deviceOID: latestSelectedDeviceOID},
            complete: function() {
                var buttonToRemove = $("button[data-deviceoid='" + latestSelectedDeviceOID +"']");
                var itemElement = buttonToRemove.closest(".items");
                var dateElementToChange = itemElement.find("#rDateMissing").first();
                var currentDate = new Date();
                dateElementToChange.text(toDateString(currentDate));
                buttonToRemove.remove();
            }
        });
    });
}
$(initContent);


function addPagination() {
    var paginationPoint = $('.pagination');

    $.get("../adminPaginationElement.hbs", function(pe) {
        // Previous page button (initially disabled) - Saved for later enabling/disabling etc.
        pgHbsElement = Handlebars.compile(pe);
        var htmlElement = pgHbsElement({
            elementId: "prev",
            id: 0,
            visibleText: "<i class=\"glyphicon glyphicon-arrow-left\"></i>"
        });
        paginationPoint.append(htmlElement);
        buttonPrev = $("#prev").closest("li");
        buttonPrev.addClass("disabled");

        // Dots "button" indicating that there's more pages than shown (before a current page) - Initially hidden
        htmlElement = pgHbsElement({
            elementId: "dotsPrev",
            initialClass: "disabled",
            visibleText: "..."
        });
        paginationPoint.append(htmlElement);
        dotsPrev = $("#dotsPrev").closest("li");
        dotsPrev.addClass("hidden");

        // Add the pages we want to display - Limited by the variable value in the top of the document
        var numberOfInitialPages = (numberOfPages > paginationRadius ? paginationRadius + 1 : numberOfPages);
        for (var i = 1; i <= numberOfInitialPages; i++) {
            htmlElement = pgHbsElement({
                elementId: "pageNumber" + i,
                id: i,
                visibleText: i
            });
            paginationPoint.append(htmlElement);
        }

        // Make the first page the active one
        $("a[data-pagelink='1']").closest("li").addClass('active');

        // Dots "button" indicating that there's more pages than shown (after a current page)
        htmlElement = pgHbsElement({
            elementId: "dotsNext",
            initialClass: "disabled",
            visibleText: "..."
        });
        paginationPoint.append(htmlElement);
        dotsNext = $("#dotsNext").closest("li");
        // Show only if it's relevant
        if (numberOfPages <= paginationRadius + 1) {
            dotsNext.addClass("hidden");
        }

        // Next page button - Saved for later enabling/disabling etc.
        htmlElement = pgHbsElement({
            elementId: "next",
            id: 2,
            visibleText: "<i class=\"glyphicon glyphicon-arrow-right\"></i>"
        });
        paginationPoint.append(htmlElement);
        buttonNext = $("#next").closest("li");


        // Listen for clicks on the pagination buttons
        paginationPoint.on('click', '.paginationButton', function(e) {
            // Prevent default behaviour and get selected page
            e.preventDefault();
            var pageNrClicked = parseInt($(this).data('pagelink'));

            // Check we are not outside available area
            if (pageNrClicked > 0 && pageNrClicked <= numberOfPages) {
                // Check that the button is not disabled or currently active
                var parent = $(this).closest("li");
                if (!parent.hasClass("disabled") && !parent.hasClass("active")) {
                    // Change what previous and next page link to
                    buttonPrev.children().first().data('pagelink', (pageNrClicked - 1));
                    buttonNext.children().first().data('pagelink', (pageNrClicked + 1));

                    // Update page buttons
                    updatePageButtons(pageNrClicked);

                    // Show/hide "dot-buttons" illustrating that more pages exists
                    if (pageNrClicked > paginationRadius + 1) {
                        dotsPrev.removeClass("hidden");
                    } else {
                        dotsPrev.addClass("hidden");
                    }
                    if (pageNrClicked < numberOfPages - paginationRadius) {
                        dotsNext.removeClass("hidden");
                    } else {
                        dotsNext.addClass("hidden");
                    }

                    // Change the visually active page
                    currentPageLbl.text(pageNrClicked);
                    parent.siblings().removeClass('active');
                    if ($(this).attr("id") != "prev" && $(this).attr("id") != "next") {
                        parent.addClass('active');
                    } else {
                        $("#pageNumber" + pageNrClicked).closest("li").addClass('active');
                    }

                    // Disable/enable previous/next buttons
                    if (pageNrClicked == 1) {
                        buttonPrev.addClass("disabled");
                    } else {
                        buttonPrev.removeClass("disabled");
                    }
                    if (pageNrClicked == numberOfPages) {
                        buttonNext.addClass("disabled");
                    } else {
                        buttonNext.removeClass("disabled");
                    }

                    // Save current page number and add the LRs for the current page
                    currentPage = pageNrClicked;
                    addLendingRegistrations(true);
                }
            }
        });
    });
}

function updatePageButtons(pageNumClicked) {
    var diff = pageNumClicked - currentPage;
    var removeIndex, lastRemove, addIndex, lastAdd;
    var htmlElement;

    // Case for bigger page number or smaller page number than current
    if (diff > 0) {
        // Remove buttons
        removeIndex = pageNumClicked - paginationRadius - 1;
        lastRemove = currentPage - paginationRadius;
        while (removeIndex >= lastRemove && removeIndex > 0) {
            $("#pageNumber" + removeIndex).closest("li").remove();
            removeIndex--;
        }
        // Add buttons
        addIndex = pageNumClicked + paginationRadius - diff + 1;
        lastAdd = pageNumClicked + paginationRadius;
        while (addIndex <= lastAdd && addIndex <= numberOfPages) {
            htmlElement = pgHbsElement({
                elementId: "pageNumber" + addIndex,
                id: addIndex,
                visibleText: addIndex
            });
            $(htmlElement).insertBefore(dotsNext);
            addIndex++;
        }

    } else {

        // Remove buttons
        removeIndex = pageNumClicked + paginationRadius + 1;
        lastRemove = pageNumClicked + paginationRadius - diff;
        while (removeIndex <= lastRemove && removeIndex <= numberOfPages) {
            $("#pageNumber" + removeIndex).closest("li").remove();
            removeIndex++;
        }

        // Add buttons
        addIndex = currentPage - paginationRadius - 1;
        lastAdd = pageNumClicked - paginationRadius;
        while (addIndex >= lastAdd && addIndex > 0) {
            htmlElement = pgHbsElement({
                elementId: "pageNumber" + addIndex,
                id: addIndex,
                visibleText: addIndex
            });
            $(htmlElement).insertAfter(dotsPrev);
            addIndex--;
        }
    }
}

function addLendingRegistrations(fade) {
    var type;
    var img;
    var startPoint = lrs.length - 1 - lrsPrPage * (currentPage - 1);

    // Remove all elements already displayed
    addPoint.fadeOut((fade ? 200 : 0), function() {
        addPoint.empty().show();

        // Add new elements
        if (lrs.length == 0) {
            addPoint.append("<br><h4 class=\"wellSpacing\">Der er ingen registrerede lån for denne medarbejder endnu</h4>");
        } else {
            var i = lrs.length - 1 - lrsPrPage * (currentPage - 1);
            while (i > (startPoint - lrsPrPage) && i >= 0) {
                if (lrs[i].device.telephoneNr) {
                    type = "Telefon";
                    img = "iphone.png";
                } else {
                    type = "Tablet";
                    img = "ipad.png";
                }
                var compiled = Handlebars.compile(lrHbsElement);
                var html = compiled({
                    lrOID: lrs[i]._id,
                    imageName: img,
                    deviceOID: lrs[i].deviceOID,
                    deviceType: type,
                    telephoneNumber: lrs[i].device.telephoneNr,
                    deviceID: lrs[i].device.deviceID,
                    lDate: toDateString(lrs[i].lendingDate),
                    rDate: toDateString(lrs[i].returnDate)
                });
                $(html).hide().appendTo(addPoint).fadeIn(200);
                i--;
            }
        }
    });
}

function toDateString(isoDate) {
    if (isoDate) {
        var date = new Date(isoDate);
        var year = date.getFullYear();
        var month = date.getMonth() < 10 ? "0" + date.getMonth() : date.getMonth();
        var day = date.getDate() < 10 ? "0" + date.getDate() : date.getDate();
        var hour = date.getHours() < 10 ? "0" + date.getHours() : date.getHours();
        var minute = date.getMinutes() < 10 ? "0" + date.getMinutes() : date.getMinutes();
        return day + "/" + month + "/" + year + " " + hour + ":" + minute;
    } else {
        return undefined;
    }
}