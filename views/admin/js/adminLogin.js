$(document).ready(function() {
    $('form').on('submit', function(event) {
        event.preventDefault();
        var output = $("#output");
        var user = $("#aUser").val();
        var pass = $("#aPass").val();
        if (user != "") {
            if (pass != "") {
                $.ajax({url: '/admin',
                        type: 'POST',
                        data: $(this).serialize(),
                        statusCode: {
                            200: function () {
                                output.addClass("alert alert-success animated fadeInUp").html("Logger " + "<span style='text-transform:uppercase'>" + user + "</span> ind..");
                                output.removeClass('alert-danger');
                                window.location.href="/admin/employees";
                            },
                            401: function () {
                                output.removeClass('alert alert-success');
                                output.addClass("alert alert-danger animated fadeInUp").html("Kun brugere med administratorrettigheder kan logge ind her!");
                            },
                            550: function () {
                                output.removeClass('alert alert-success');
                                output.addClass("alert alert-danger animated fadeInUp").html("Det indtastede indianernavn og password giver ikke administrator adgang...");
                            }
                        }
                    }
                );
            } else {
                output.removeClass('alert alert-success');
                output.addClass("alert alert-danger animated fadeInUp").html("Indtast et password");
            }
        } else {
            output.removeClass('alert alert-success');
            output.addClass("alert alert-danger animated fadeInUp").html("Indtast et indianernavn");
        }
    });
});