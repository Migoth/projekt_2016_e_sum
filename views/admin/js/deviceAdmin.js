function initContent() {
    renderDevices();

    $("#addButton").on("click", function(){
       window.location.replace("/admin/edit/device");
    });

    function renderDevices(){
        $.get("adminDevice.hbs", function(device){
            $.getJSON("/api/admin/devices", function (data) {
                for (var i = data.length - 1; i >= 0; i--) {
                    var type;
                    var img;
                    if (data[i].telephoneNr){
                        type = "Telefon";
                        img = "iphone.png";
                    } else {
                        type = "Tablet";
                        img = "ipad.png";
                    }
                    var compiled = Handlebars.compile(device);
                    var html = compiled({
                        id:data[i].deviceID,
                        _id:data[i]._id,
                        type: type,
                        telephoneNumber: data[i].telephoneNr,
                        imageName: img,
                        serialNumber: data[i].serialNr
                    });
                    $('#deviceAdmin').append(html);
                }

                $(".selectableRow").on("click", function(){
                    window.location.href = "/admin/device/" + $(this).data("oid");
                });
            });
        });
    }
    $('#searchInput').on("keypress",function(event) {
        if(event.keyCode == 13  ||event.keyCode == 10){
            event.preventDefault()
        }
    });
}
$(initContent);

function searchList(seed){
    var regex =new RegExp("[a-z0-9  øæåØÆÅ]+","i");
    if (seed!=""&& regex.test((seed))) {
        regex = new RegExp(seed+"+","i");
    }
    var items = $('.items');
    for (var i=0;i<items.length;i++){
        var info=$('#'+items[i].id+' .deviceInfo');
        if ( regex.test(info[info.length-1].innerText)||regex.test(info[info.length-2].innerText)){
            items[i].style.display= 'block';
        }else{
            items[i].style.display = 'none';
            items.splice(i, 1);
            i--;
        }
    }
    return items;
}


function removeFocus(){
    $('#searchBtn').blur();
}