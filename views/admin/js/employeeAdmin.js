function initContent() {
    // Populate table initially
    $.get("adminEmployee.hbs", function(employee) {
        $.getJSON("/api/admin/employees", function (data) {
            for (var i = data.length - 1; i >= 0; i--) {
                var compiled = Handlebars.compile(employee);
                var html = compiled({
                    id:i,
                    _id: data[i]._id,
                    name: data[i].name,
                    indianName:data[i].password,
                    imageName: data[i].picture ? data[i].picture : "silhouette.png"
                });
                $('#employeeAdmin').append(html);
            }

            $(".selectableRow").click(function() {
                console.log("log");
                window.location.href = "/admin/employee/" + $(this).data("oid");
            });
        });

        $("#newEmployee").click(function() {
            window.location.href = "/admin/edit/employee";
        });
    });
    $('#searchInput').on("keypress",function(event) {
        if(event.keyCode == 13  ||event.keyCode == 10){
            event.preventDefault()
        }
    });
}
$(initContent());

function searchListPeople(seed) {
    var regex = new RegExp("[A-Za-z0-9ØÆÅøæå]+");
    if (seed != "" && regex.test((seed))) {
        regex = new RegExp(seed + "+", 'i');
    }
    var items = $('.employee');
    for (var i=0; i<items.length; i++){
        var info = $('#' + items[i].id + ' .personInfo');
        if (regex.test(info[0].innerHTML) || regex.test(info[1].innerHTML)){
            items[i].style.display= 'block';
        } else {
            items[i].style.display = 'none';
            items.splice(i, 1);
            i--;
        }
    }
    return items;
}

function removeFocus() {
    $('#searchBtn').blur();
}