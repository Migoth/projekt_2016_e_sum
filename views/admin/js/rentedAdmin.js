var devices;
var methodToUse;
var latestSelectedDeviceOID;

function initContent() {
    // Populate table initially
    $.get("adminRent.hbs", function(Rent){
        $.getJSON("/api/lendingregistrations", function (data) {
            for (var i = data.length - 1; i >= 0; i--) {
                var type;
                var img;
                if (data[i].device.telephoneNr){
                    type = "Telefon";
                    img = "iphone.png";
                } else {
                    type = "Tablet";
                    img = "ipad.png";
                }
                var compiled = Handlebars.compile(Rent);
                var html = compiled({
                    rowID:i,
                    imageName: data[i].user.picture ? "/employees/"+data[i].user.picture : "/employees/silhouette.png",
                    name:data[i].user.name,
                    id:data[i].device.deviceID,
                    type: type,
                    telephoneNumber: data[i].device.telephoneNr,
                    deviceOID: data[i].deviceOID
                });
                $('#rentAdmin').append(html);
            }

            // Listen for "Return device" button clicks
            $("#rentAdmin").on("click", ".inListBtn", function() {
                latestSelectedDeviceOID = $(this).data('deviceoid');
                $("#modalText").text("Vil du aflevere enheden med id " + $(this).data('deviceid') + "?");
                $("#deviceModal").modal("show");
            });
        });

        // Button for confirming a return of a device
        $("#confirmReturnButton").on("click", function() {
            $.ajax({
                url: "/api/lendingregistrations",
                type: "PUT",
                dataType: "json",
                data: {deviceOID: latestSelectedDeviceOID},
                complete: function() {
                    var buttonToRemove = $("button[data-deviceoid='" + latestSelectedDeviceOID +"']");
                    var itemElement = buttonToRemove.closest(".items");
                    itemElement.fadeOut(1000);
                }
            });
        });
    });
    $('#searchInput').on("keypress",function(event) {
        if(event.keyCode == 13  ||event.keyCode == 10){
            event.preventDefault()
        }
    });
}
$(initContent());

//Modal click handler for confirmation
$("#confirmButton").on("click", function(){
    methodToUse();
});

function searchList(seed){
    var regex =new RegExp("[a-z0-9øæåØÆÅ]+",'i');
    if (seed!=""&& regex.test((seed))) {
        regex = new RegExp(seed+"+",'i');
    }
    var items = $('.items');
    for (var i = items.length - 1; i >= 0; i--){
        var info=$('#'+items[i].id+' .rentInfo');

        if ( regex.test(info[0].innerText )|| regex.test(info[1].innerText) ||  regex.test(info[2].innerText)){
            items[i].style.display= 'block';
        }else{
            items[i].style.display = 'none';
        }
    }
    return items;
}
function removeFocus(){
    $('#searchBtn').blur();
}