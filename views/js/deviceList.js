var phoneID;
var tabletID;
var tablets;
var phones;
var confirming = false;


function searchDeviceID(target, collection){
    var i = 0;
    while(i < collection.length){
        if (collection[i].deviceID == target){
            return collection[i]._id;
        } else {
            i++;
        }
    }
    return "";
}

//Click handlers for the render method. Varies whether the list contains phones or tablets.
var phoneClickHandler = function(){
    phoneID = $(this).parent().attr("id");
    $("#rentAndContinue").show();
    $("#modalText").text("Vil du låne telefonen med id " + phoneID + "?");
    $("#deviceModal").modal("show");
};

var tabletClickHandler = function(){
    tabletID = $(this).parent().attr("id");
    $("#rentAndContinue").hide();
    $("#modalText").text("Vil du låne telefonen med id " + phoneID + " og tablet med id " + tabletID + "?");
    $("#deviceModal").modal("show");
};

var confirm = function(){
    $("#rentAndContinue").hide();
    $("#modalText").html("<b>Advarsel!</b><br>Du forsøger at låne en enhed, der <i>allerede er registreret som udlånt</i>. Ønsker du at fortsætte alligevel?");
    $("#deviceModal").modal("show");
};

var checkRentalState = function () {
    var phoneDatabaseID = searchDeviceID(phoneID, phones);
    var tabletDatabaseID;
    if (tabletID){
        tabletDatabaseID = searchDeviceID(tabletID, tablets);
    }


    //Post the registration of the phone
    $.ajax({
        url: '/api/lendingregistrations/lentdevices',
        type: "POST",
        data: {phoneID: phoneDatabaseID, tabletID: tabletDatabaseID},
        dataType: "json",
        statusCode: {
            204: function(){
                rental();
            },
            500: function(){
                alert("Noget gik galt... Kunne ikke modtage data fra databasen.");
            },
            400: function() {
                $("#confirm").unbind();
                $("#confirm").on("click", rental);
                confirming = true;
            }
        }
    });

};

var rental = function(){
    var phoneDatabaseID = searchDeviceID(phoneID, phones);
    $.post("/api/lendingregistrations", {deviceOID: phoneDatabaseID}, function(){
        //If tablet is chosen, post another registration
        if (tabletID){
            var tabletDatabaseID = searchDeviceID(tabletID, tablets);
            $.post("/api/lendingregistrations", {deviceOID: tabletDatabaseID}, function(){
                window.location.replace("/lendingLogin");
            });
        } else {
            window.location.replace("/lendingLogin");
        }
    });
}

$(initContent);

function initContent() {

    $.getJSON("/api/devices/tablets", function(data){
        tablets = data;
    });

    $.getJSON("/api/devices/phones", function(data){
        phones = data;
        renderDevices(phones, phoneClickHandler);
    });
    $("#deviceModal").on("hidden.bs.modal", function(){
        if (confirming){
            confirming = false;
            confirm();
        }
    });

    //renderDevices("phones", phoneClickHandler);
    //Modal button event - rent and log out
    $("#confirm").on("click", checkRentalState);


    //Modal button event - rent and continue to rent tablet
    $("#rentAndContinue").on("click", function () {
        $("#devices").slideUp().promise().then(function () {
            $("#devices").empty();
            $("#queue").val("");
            $("#deviceTitle").text("Vælg en tablet");
            renderDevices(tablets, tabletClickHandler);
        });
    });


    //Modal button event - cancel
    $("#cancel").on("click", function () {
       //Reset confirm button event
        $("#confirm").unbind();
        $("#confirm").on("click", checkRentalState);
        //Resets the interface if necessary
        if (tabletID) {
            tabletID = "";
            phoneID = "";

            //Renders the list of phones anew
            $("#devices").slideUp().promise().then(function () {
                $("#devices").empty();
                $("#queue").val("");
                $("#deviceTitle").text("Vælg en telefon");
                renderDevices(phones, phoneClickHandler);
            });
        }
    });
    $('#searchInput').on("keypress",function(event) {
        if(event.keyCode == 13  ||event.keyCode == 10){
            event.preventDefault()
        }
    });
}

function renderDevices(toRender, clickHandler){
    var deviceType;
    var image;
    if (!phoneID){
        deviceType = "Telefon";
        image = "iphone";
    } else {
        deviceType = "Tablet";
        image = "ipad";
    }
    $.get("device.hbs", function(device){
        for (var i = toRender.length - 1; i >= 0; i--){
            var compiled = Handlebars.compile(device);
            var html = compiled({
                deviceID: toRender[i].deviceID,
                deviceType:  deviceType,
                imageName: image,
                //Phone number does not get rendered if the value is falsy
                telephoneNumber: toRender[i].telephoneNr
            });
            $('#devices').append(html);
            $("#devices").slideDown();
        }
        $(".selectableRow").click(clickHandler);
    });
}

function searchList(seed){
    var regex =new RegExp("[a-z0-9øæåØÆÅ]+","i");
    if (seed!=""&& regex.test((seed))) {
        regex = new RegExp(seed+"+","i");
    }
    var items = $('.items');
    for (var i=0;i<items.length;i++){
        var info=$('#'+items[i].id+' .productInfo');
        if ( regex.test(info[info.length-1].innerHTML)||regex.test(info[info.length-2].innerHTML)){
            items[i].style.display= 'block';
        }else{
            items[i].style.display = 'none';
            items.splice(i, 1);
            i--;
        }
    }
    return items;
}

function removeFocus(){
    $('#searchBtn').blur();
}