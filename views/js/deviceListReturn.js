var devices;
var selectedDeviceID;
var methodToUse;

//Method for returning single device
function returnSingleDev(){
    var databaseID = findDatabaseID();
    console.log(databaseID);
    var request = $.ajax({
        url: "api/lendingregistrations",
        type: "PUT",
        dataType: "json",
        data: {deviceOID: databaseID},
        complete: renderDevices
    });
}

//Method for returning all devices
function returnAllDevs(){
    var doneRequests = 0;
    for (var i = 0; i < devices.length; i++){
        $.ajax({
            url: "api/lendingregistrations",
            type: "PUT",
            dataType: "json",
            data: {deviceOID: devices[i].deviceOID},
            complete: function(){
                doneRequests++;
                if(doneRequests == devices.length){
                    $("#devicesReturn").empty();
                }
            }
        })
    }
}

//Click handler for the generated buttons
function returnSingleDevice(){
    selectedDeviceID = $(event.target).attr("id");
    methodToUse = returnSingleDev;
    $("#modalText").text("Vil du aflevere enheden med id " + selectedDeviceID + "?");
    $("#deviceModal").modal("show");
}

//Used to find the database ID of the selected device
function findDatabaseID(){
    var i = 0;
    while(i < devices.length){
        if (selectedDeviceID == devices[i].device.deviceID){
            return devices[i].deviceOID;
        } else {
            i++;
        }
    }
    return "";
}

function initContent() {
    renderDevices();

    //Click handler for renting another device
    $("#rentMore").on("click", function(){
        window.location.replace("/lending");
    });

    //Click handler for returning all devices
    $("#returnAll").on("click", function(){
            methodToUse = returnAllDevs;
            $("#modalText").text("Vil du aflevere alle lånte enheder?");
            $("#deviceModal").modal("show");

    });

    //Modal click handler for confirmation
    $("#confirmButton").on("click", function(){
        methodToUse();
    });


}

function renderDevices(){
    $("#devicesReturn").empty();
    $.getJSON('/api/lendingregistrations/fromuser', function(data){
        //Save the devices to be able to search through them later
        devices = data;
        // Populate table initially
        $.get("deviceReturn.hbs", function(device){
            for (var i = data.length - 1; i >= 0; i--) {
                var devType;
                var img;
                if (data[i].device.telephoneNr){
                    devType = "Telefon";
                    img = "iphone";
                } else {
                    devType = "Tablet";
                    img = "ipad";
                }
                var compiled = Handlebars.compile(device);
                var html = compiled({
                    deviceID: data[i].device.deviceID,
                    deviceType: devType,
                    imageName: img,
                    telephoneNumber: data[i].device.telephoneNr
                });
                $('#devicesReturn').append(html);
            }
        });
    });
}


$(initContent);