function initContent() {
    // Populate table initially
    $.get("employee.hbs", function(employee){
        $.getJSON("/api/lendingregistrations/phones", function (data) {
            for (var i = data.length - 1; i >= 0; i--) {
                var compiled = Handlebars.compile(employee);
                var html = compiled({
                    id:i,
                    name: data[i].user.name,
                    title: data[i].user.title,
                    phoneNumber: data[i].device.telephoneNr,
                    imageName: data[i].user.picture ? data[i].user.picture : "silhouette.png"
                });
                $('#employees').append(html);
            }
            $(".selectableRow").click(function() {
                console.log("Registered");
                $(this).toggleClass("deviceRowSelected");
            });

            $('#searchInput').on("keypress",function(event) {
                if(event.keyCode == 13  ||event.keyCode == 10){
                    event.preventDefault()
                }
            });

        });
    });
    $('#searchInput').on("keypress",function(event) {
        if(event.keyCode == 13  ||event.keyCode == 10){
            event.preventDefault()
        }
    });
}
$(initContent());

function searchListPeople(seed){
    var regex =new RegExp("[A-Za-z0-9ØÆÅøæå]+");
    if (seed!=""&& regex.test((seed))) {
        regex = new RegExp(seed+"+",'i');
    }
    var items = $('.employee');
    for (var i=0;i<items.length;i++){
        var info=$('#'+items[i].id+' .personInfo');
        if ( regex.test(info[0].innerText) || regex.test(info[1].innerText) || regex.test(info[2].innerText)){
            items[i].style.display= 'block';
        }else{
            items[i].style.display = 'none';
        }
    }
    return false;
}


function removeFocus(){
    $('#searchBtn').blur();
}