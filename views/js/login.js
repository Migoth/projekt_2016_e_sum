$(document).ready(function() {
    $('form').on('submit', function(event) {
        event.preventDefault();
        var output = $("#output");
        var user = $("#user").val();
        var url = "/api/lendingregistrations/fromuser";

        if (user != "") {
            $.ajax({url: '/',
                    type: 'POST',
                    data: 'name='+user,
                    statusCode: {
                        200: function () {
                            output.addClass("alert alert-success animated fadeInUp").html("Logger " + "<span style='text-transform:uppercase'>" + user + "</span> ind..");
                            output.removeClass('alert-danger');
                            if(location.pathname == "/") {
                                window.location.href = "/employees";
                            } else {
                                $.getJSON(url, function (json) {
                                    if (json.length == 0) {
                                        window.location.href = "/lending";
                                    }
                                    else {
                                        window.location.href = "/return";
                                    }
                                });
                            }
                        },
                        550: function () {
                            output.removeClass('alert alert-success');
                            output.addClass("alert alert-danger animated fadeInUp").html("Indianernavnet '" + user + "' blev ikke fundet...");
                        }
                    }
                }
            );
        } else {
            output.removeClass('alert alert-success');
            output.addClass("alert alert-danger animated fadeInUp").html("Indtast et indianernavn");
        }
    });
});